from flask import Flask, render_template, request
from flask_mysqldb import MySQL
app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_PORT'] = 3307
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'rty52552551'
app.config['MYSQL_DB'] = 'test'   
print('db connected sucessfully')
mysql = MySQL(app)

@app.route('/')
def index():
    return render_template('index.html')
@app.route('/testtable', methods = ['GET'])
def testtable():            
    cur = mysql.connection.cursor()                 #cursor創建並返回游標
    cur.execute('SELECT * FROM testtable')
    data = cur.fetchall()                           #獲取結果集中剩下的所有行
    cur.close()
    return render_template('testtable.html',data = data)
if __name__ == '__main__':
    app.run()

